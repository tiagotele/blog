class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy
	belongs_to :post
	validates_presence_of :title
	validates_presence_of :body
end
